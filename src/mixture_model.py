import numpy as np;
import maths;

class mixture_model: 
    def __init__(self,theta,per_class_models):
        self.logtheta = np.log(theta);
        self.per_class_models = per_class_models;

    def predict(self,x):
        n_predictions = x.shape[0];
        n_classes = self.logtheta.size;

        # Initialize with the base probabilities
        logClassPrior = self.logtheta;
        logPredictions = np.zeros([n_predictions,n_classes]);
        logPredictions = logPredictions + logClassPrior;
        for class_i in range(n_classes):
            r = self.per_class_models[class_i].logpdf(x);
            #%s1 = size(logPredictions(:,class_i))
            logPredictions[:,class_i] = logPredictions[:,class_i]+ r;
        logPredictions = maths.normalizeInLogDomain(logPredictions);
        return logPredictions;

