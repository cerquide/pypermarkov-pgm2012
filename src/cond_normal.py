from __future__ import division;
import numpy as np;
import scipy.stats as ss;
import scipy.linalg as linalg;
import math;

import maths;
import mvn;

class cond_normal: 
    def __init__(self,mu,beta,sigma_2):
        self.n_condicionants = beta.shape[0];
        self.mu = mu;
        self.beta = beta;
        self.sigma_2 = sigma_2;

    def logcpdf(self,x,y):
        if (self.n_condicionants == 0):
            #print self.mu
            #print self.sigma_2
            #logp = mvn.logpdf(x,self.mu,self.sigma_2);
            logp = ss.norm.logpdf(y,loc=self.mu,scale=math.sqrt(self.sigma_2));
        else:
            #print self.beta
            #print x
            cbx = np.dot(self.beta,x.transpose());
            pmu = self.mu + cbx;
            #%logp = (log(normpdf(y',pmu,sqrt(cn.sigma_2))))
            #print y
            #print pmu
            #print self.sigma_2
            logp = ss.norm.logpdf(y,loc=pmu,scale=math.sqrt(self.sigma_2));
            #logp2 = mvn.logpdf(y,pmu,self.sigma_2);
            #print logp-logp2;
        return logp;

def learn_ml(x,y):
    #print x
    n_condicionants = x.shape[1];
    if (n_condicionants == 0):
        #np.cov(y,rowvar=0)
        return cond_normal(np.mean(y,axis=0),np.array([]),maths.cov_always_matrix(y));
    else:
        #print x.shape =
        #print y.shape
        z = np.hstack((x, y));
        sigmaTmp = maths.cov_always_matrix(z);
        #print np.cov(z,rowvar=0);
        #print np.cov(z,rowvar=1);
        #print z
        #print 'sigmaTmp=',sigmaTmp;
        sigmaAA = sigmaTmp[0:n_condicionants,0:n_condicionants];
        sigmaBA = sigmaTmp[[-1],0:n_condicionants];
        beta = np.ravel(linalg.lstsq(sigmaAA,sigmaBA.transpose())[0]);
        mx1 = np.mean(x,axis=0);
        #print np.mean(y,axis=0).shape
        #print beta
        #print mx1
        #print np.dot(beta.transpose(),mx1).shape
        mu = np.mean(y,axis=0) - np.dot(beta,mx1);
        sigma_2 = max(sigmaTmp[-1,-1] - np.dot(beta,sigmaBA.T),0);
        return cond_normal(mu,beta,sigma_2);
  


