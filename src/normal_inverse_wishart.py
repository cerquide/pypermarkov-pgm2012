from __future__ import division;
import numpy as np;

import maths;
import mvt;
from distributions import *;

class normal_inverse_wishart(conjugate_distribution):
    def __init__(self,mu,k,nu,llambda):
        #print mu.shape;
        self.dim = mu.shape[0];
        self.mu = mu;
        self.k = k;
        self.nu = nu;
        self.llambda = llambda;

    def incorporate_evidence(self,x):
        n_instances, n_vars = x.shape;
       
        #print 'mu = ',self.mu;
        var_means = np.mean(x,0);
        #print 'var_means =', var_means;
        m_d = var_means - self.mu;
        posterior_k = self.k + n_instances;
        #mean_matrix = repmat(var_means,n_instances,1);
        #print 'x=',x;
        N = x - var_means;
        #print 'N=',N;
        S = np.dot(N.transpose(),N);
        #print 'S=',S;
        r = (self.k * n_instances)/posterior_k;
        #print 'lambda=',self.llambda;
        #self.llambda = maths.make_symmetric(self.llambda + S + r*(np.dot(m_d.transpose(),m_d)));
        self.llambda = self.llambda + S + r*(np.dot(m_d.transpose(),m_d));
        #print 'lambda=',self.llambda;
        self.mu = (self.k * self.mu + n_instances * var_means) / posterior_k;
        self.nu = self.nu + n_instances;
        self.k = posterior_k;
        #print self

    def get_posterior_predictive_distribution(self):
        #print self;
        nu_ = self.nu - self.dim + 1;
        Sigma_ = self.llambda * ((self.k + 1) / (self.k * self.nu));
        #print nu_,Sigma_;
        return mvt.mvt(nu_,self.mu,Sigma_);
       

    def logpdf(self, x):    
        #Yet to be implemented
        pass

    def __str__(self):
        x = '[ dim = ' + str(self.dim);
        x = x + ' mu = ' + str(self.mu);
        x = x + ' k=' + str(self.k);
        x = x + ' nu = ' + str(self.nu);
        x = x + ' lambda = ' +str(self.llambda);
        x = x + ']\n';
        return x;

def prior_1(dim):
    return normal_inverse_wishart(np.zeros(dim),1,dim-1,np.eye(dim));

def prior_2(dim,nu):
    return normal_inverse_wishart(np.zeros(dim),0,nu,np.eye(dim));
 

