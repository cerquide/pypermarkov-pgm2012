import scipy.linalg as linalg;
import numpy as np;

def quadratic_form_of_inverse(M,x):
    tmp1 = linalg.lstsq(M,x.transpose())[0];
    return (tmp1 * x.transpose()).sum(0);
       
def make_symmetric(M):
    return np.triu(M)+ np.triu(M,1).transpose()

def cov_always_matrix(x):
    if x.shape[0] == 1:
        sigma = np.zeros([x.shape[1],x.shape[1]]);
    else:
        sigma = np.cov(x,rowvar=0);
    return sigma

def normalizeInLogDomain(logpp):
    return logpp - sumInLogDomain(logpp);

def sumInLogDomain(logpp):
    max_p = np.amax(logpp,axis=1);
    max_p.shape = (max_p.size,1);
    logpp = logpp - max_p;
    epp = np.exp(logpp);
    s = np.sum(epp,axis=1);
    s.shape = (s.size,1);
    return max_p+np.log(s);

