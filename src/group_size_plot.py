import experiments
import matplotlib.pyplot as plt
import re
import numpy as np

def num_vars(num_classes,num_vars,classifier_type,group_sizes):
    if classifier_type.endswith('JAN'):
        return num_classes*num_vars*(group_sizes+(group_sizes*(group_sizes+1))/2)/group_sizes
    else:
        return num_classes*(group_sizes+1)*(num_vars-(group_sizes*(group_sizes-1))/2)       

labels={}
labels['ML-K-JAN']= 'CGN-K-BOX';
labels['ML-K-BAND']= 'CGN-K-BAND';
labels['BMA-K-JAN']= 'GJT-K-BOX';
labels['BMA-K-BAND']= 'GJT-K-BAND';
measureLabels={}
measureLabels['cll']= 'Conditional log likelihood';
measureLabels['accuracy']= 'Accuracy';
measureLabels['learning_time']= 'Learning time';
measureLabels['prediction_time']= 'Prediction time';

def plot_comparison_along_num_vars_ovarian(task,classifier_types,measures,results):
    for (m_i,measure) in enumerate(measures):
        f = plt.figure();
        #f_errorbar = figure();
        colors=['r:','b--','g-.','k'];
        i = 0;
        for classifier_type in classifier_types:
            group_sizes = determine_group_sizes(results,classifier_type);
            print group_sizes
            m = get_measure(m_i,classifier_type,results,group_sizes);
            m2 = np.average(m,axis=2);
            print m2
            m3 = np.average(m2,axis=1);
            print m3
            nvs = num_vars(2,11300,classifier_type,np.array(group_sizes))
            print 'nvs',nvs
            plt.plot(nvs,m3,colors[i],label=labels[classifier_type])
            i = i + 1 
        plt.ylabel(measureLabels[measure])
        plt.xlabel('Number of variables in the model')
        if measure=='cll':
            plt.ylim((-1600,-200))
        elif measure=='accuracy':
            plt.ylim((0.7,0.95))
        plt.legend(loc='upper right')
        plt.savefig(task.name+'-'+measure+'-nv.pdf');
        plt.show() 

def plot_comparison_along_num_vars_pancreas(task,classifier_types,measures,results):
    for (m_i,measure) in enumerate(measures):
        f = plt.figure();
        #f_errorbar = figure();
        colors=['r:','b--','g-.','k'];
        i = 0;
        for classifier_type in classifier_types:
            group_sizes = determine_group_sizes(results,classifier_type);
            print group_sizes
            m = get_measure(m_i,classifier_type,results,group_sizes);
            m2 = np.average(m,axis=2);
            print m2
            m3 = np.average(m2,axis=1);
            print m3
            nvs = num_vars(2,6771,classifier_type,np.array(group_sizes))
            print 'nvs',nvs
            plt.plot(nvs,m3,colors[i],label=labels[classifier_type])
            i=i+1
        plt.ylabel(measureLabels[measure])
        plt.xlabel('Number of variables in the model')
        if measure=='cll':
            plt.ylim((-10000,-1500))
            plt.legend(loc='lower right')
        else:
            plt.legend(loc='upper right')
        plt.savefig(task.name+'-'+measure+'-nv.pdf');
        plt.show() 

def plot_comparison_along_groupsize(task,classifier_types,measures,results):
    for (m_i,measure) in enumerate(measures):
        f = plt.figure();
        #f_errorbar = figure();
        colors={'r','b'};
        i = 1;
        for classifier_type in classifier_types:
            group_sizes = determine_group_sizes(results,classifier_type);
            print group_sizes
            m = get_measure(m_i,classifier_type,results,group_sizes);
            m2 = np.average(m,axis=2);
            print m2
            m3 = np.average(m2,axis=1);
            print m3
            plt.plot(group_sizes,m3,label=classifier_type)
        plt.legend(loc='lower center')
        plt.savefig(task.name+'.'+measure+'.pdf');
        plt.show() 
   
def get_measure(m_i,classifier_type,results,group_sizes):
    n_measures,n_folds,n_runs = results[results.keys()[0]].shape
    m = np.zeros([len(group_sizes),n_folds,n_runs])
    for (group_size_i,group_size) in enumerate(group_sizes):
        m[group_size_i,:,:] = results[classifier_type+'-'+str(group_size)][m_i,:,:]
    return m
#        avg_measure_run = reshape(measure,numel(group_sizes),runs*folds);
#        avg_measure = mean(avg_measure_run,2);
#        prcs = prctile(avg_measure_run,[40,50,60],2);
#        median = prcs(:,2)';
#        lower = prcs(:,2)' - prcs(:,1)';
#        upper = prcs(:,3)' - prcs(:,2)';
#        figure(f);
#        plot(group_sizes,median,colors{i});
#        hold;
#        figure(f_errorbar);
#        errorbar(group_sizes,median,lower,upper,colors{i});
#        hold;
#        i = i + 1;
#    end
#    figure(f);
#    do_legend(classifier_types,measure_name);
#    saveas(f,['fig/' file_name '.' measure_name '.median.plot.pdf']);
#    close(f);
#    figure(f_errorbar);
#    do_legend(classifier_types,measure_name);
#    saveas(f_errorbar,['fig/' file_name '.' measure_name '.errorbar.plot.pdf']);
#    close(f_errorbar);
#end

#function do_legend(classifier_types,measure_name)
#    legend(classifier_types);
#    xlabel('Size of the groups');
#    ylabel(measure_name);
#end

def determine_group_sizes(results,classifier_name):
    group_sizes = [];
    classifiers = results.keys()
    for classifier_i_name in classifiers:
        n = get_group_size(classifier_i_name,classifier_name);    
        if n > 0:
            group_sizes.append(n);
    group_sizes.sort()
    return group_sizes

def get_group_size(classifier_i_name,classifier_name):
    prog = re.compile(classifier_name + '-(\d*)');
    m = prog.match(classifier_i_name);
    if m:
        print m.group(1)
        return int(m.group(1))
    else:
        return 0
  
#function  [real_runs] = determine_real_runs(results,runs,dataset_i)
#    real_runs = runs;
#    for run = 1 : runs
#        if (numel(results.fold_results{dataset_i}{run,1}) == 0)
#            real_runs = run - 1;
#            return
#        end
#    end
#end

#function m = get_measure(measure_name,classifier_name,results,group_sizes,runs,folds,dataset_i)
#    n_groups = numel(group_sizes);
#    m = zeros(n_groups,runs,folds);
#    for run = 1 : runs
#        for fold = 1: folds
#            rf_results = results.fold_results{dataset_i}{run,fold};
#            group_i = 1;
#            for classifier_i = 1 : numel(rf_results)
#                n = get_group_size(rf_results{classifier_i}.name,classifier_name);
#                if n > 0
#                    f = getfield(rf_results{classifier_i}.metrics,measure_name);
#                    m(group_i,run,fold) = f;
#                    group_i = group_i + 1; 
#                else
#                    'No';
#                end
#            end
#        end
#    end
#end

h = experiments.experiment_handle('pgm2012','pgm2012.hdf5');
e = experiments.experiment.open(h,'r');
#t = e.get_task('1CV10.1');
#t.runs = 5;
t = e.get_task('1CV10.1');
t.runs = 5;
measures = ['accuracy','cll','learning_time','prediction_time'];
m = t.get_results(e.fileh,measures);
inducerNames =  ['ML-K-JAN','BMA-K-JAN','ML-K-BAND','BMA-K-BAND'];
plot_comparison_along_num_vars_ovarian(t,inducerNames,measures,m);
t = e.get_task('pancreas');
t.runs = 5;
measures = ['accuracy','cll','learning_time','prediction_time'];
m = t.get_results(e.fileh,measures);
plot_comparison_along_num_vars_pancreas(t,inducerNames,measures,m);
e.close();
        
       
