from __future__ import division;
import numpy as np;
#import scipy.stats as ss;
#import scipy.linalg as linalg;
#import math;
import distributions;
import cond_normal;
import normal_inverse_wishart;

class gaussian_clique_tree(distributions.strong_hypermarkov_distribution):
    def __init__(self,cliques,separators):
        distributions.strong_hypermarkov_distribution.__init__(self,cliques,separators);

def prior(dim,clique_domains,separator_domains):
    cliques = _create_factors(dim,clique_domains);
    separators = _create_factors(dim,separator_domains);
    return gaussian_clique_tree(cliques,separators);

def _create_factors(dim,factor_domains):
    n_factors = len(factor_domains);
    factors = [None] * n_factors;
    for factor_i in xrange(n_factors):
        factor_domain = factor_domains[factor_i];
        dim_distribution = len(factor_domain);
        #%nu = dim - dim_distribution + 1;
        nu = dim_distribution-1;
        cd = normal_inverse_wishart.prior_2(dim_distribution,nu);
        factors[factor_i] = distributions.conjugate_factor(dim,factor_domain,cd);
    return factors;

            
    
