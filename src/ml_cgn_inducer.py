from __future__ import division 
import numpy as np;
import math
import networkx as nx

import dataset;
import gaussian_network;
import mixture_model;
import inducers;

def naive_bayes_bn_learner(dstr,xtrain,ytrain,options):
    #print dstr;
    options['group_size'] = 1;
    return k_jan_bn_learner(dstr,xtrain,ytrain,options)
 
def k_jan_bn_learner(dstr,xtrain,ytrain,options):
    DG = nx.DiGraph();
    DG.add_nodes_from(xrange(dstr.n_vars));
    group_size = options['group_size'];
    n_groups = int(math.ceil(dstr.n_vars/group_size));  
    #print 'n_groups=',n_groups
    #parents = [None] * dstr.n_vars
    for group_i in xrange(n_groups):
        start = group_i*group_size
        end = min(dstr.n_vars,start+group_size)
        for var_i in range(start,end):
            for var_j in range(start,var_i):
                #print "addding ",var_i,"->",var_j
                DG.add_edge(var_j,var_i)
    return DG

def k_band_bn_learner(dstr,xtrain,ytrain,options):
    DG = nx.DiGraph();
    DG.add_nodes_from(xrange(dstr.n_vars));
    group_size = options['group_size'];
    #print 'n_groups=',n_groups
    #parents = [None] * dstr.n_vars
    for var_i in range(dstr.n_vars):
        start = max(0,var_i - group_size) 
        for var_j in range(start,var_i):
            DG.add_edge(var_j,var_i)
    return DG

def assess_cmi(dstr,xtrain,ytrain):
    inst_by_class = dataset.split_by_classes(dstr,xtrain,ytrain);
    
    model_theta = np.zeros(dstr.n_classes);
    for class_i in range(dstr.n_classes):
        model_theta[class_i] = inst_by_class[class_i].shape[0];
    model_theta = model_theta/np.sum(model_theta);

    mi = np.zeros([dstr.n_vars,dstr.n_vars]);
    mi_tmp = np.zeros([dstr.n_vars,dstr.n_vars]);
    for class_i in range(dstr.n_classes):
        mi_tmp = np.corrcoef(inst_by_class[class_i],rowvar=0);
        mi_tmp *= -mi_tmp
        mi_tmp += 1
        mi_tmp = np.log(mi_tmp)
        mi_tmp *= model_theta[class_i]
        mi += mi_tmp
    return mi    
    
        
#def tan_bn_learner(dstr,xtrain,ytrain,options):
#    mi = assess_cmi(dstr,xtrain,ytrain)
    
        
        
    
    
bn_learners = {};
bn_learners['naive_bayes'] = naive_bayes_bn_learner;
bn_learners['k_jan'] = k_jan_bn_learner;
bn_learners['k_band'] = k_band_bn_learner;
#bn_learners['tan'] = tan_bn_learner;

class ml_cgn_inducer(inducers.inducer):
    #STRUCTURE_LEARNER = 'STRUCTURE_LEARNER'
    #PARAMETER_LEARNER = 'PARAMETER_LEANER'
    def __init__(self,options): 
        self.options = options;
        if (not self.has_learner()):
            self.options['bn_learner'] = 'naive_bayes'; 
        #if (not self.has_parameter_learner()):
        #    self.options['parameter_learner'] = ''; 
    
    def has_learner(self):
        return self.options.has_key('bn_learner');

    def learn(self,dstr,xtrain,ytrain):
        structure = bn_learners[self.options['bn_learner']](dstr,xtrain,ytrain,self.options);
        model = self.learn_parameters(dstr,xtrain,ytrain,structure,self.options);
        return model
    
    def learn_parameters(self,dstr,xtrain,ytrain,structure,options):
        # Assess the unconditional probabilities for each class.
        model_theta = np.zeros(dstr.n_classes);
        for class_i in range(dstr.n_classes):
            model_theta[class_i] = np.count_nonzero(ytrain==dstr.class_values[class_i]);
        model_theta = model_theta/np.sum(model_theta);

        # Divide the training set in classes
        inst_by_class = dataset.split_by_classes(dstr,xtrain,ytrain);
        
        # Assess the parameters for each of the classes
        model_cgns = [None] * dstr.n_classes;

        for class_i in range(dstr.n_classes):
            model_cgns[class_i] = gaussian_network.learn_ml(inst_by_class[class_i],structure);
        return mixture_model.mixture_model(model_theta,model_cgns);
   
class ml_k_jan_inducer(ml_cgn_inducer):
    def __init__(self,group_size):
        options = {};
        options['group_size'] =  group_size;
        options['bn_learner'] = 'k_jan';
        ml_cgn_inducer.__init__(self,options);
        self.name = 'ML-K-JAN-'+str(group_size);
        #self.group_size = group_size;

class ml_k_band_inducer(ml_cgn_inducer):
    def __init__(self,group_size):
        options = {};
        options['group_size'] =  group_size;
        options['bn_learner'] = 'k_band';
        ml_cgn_inducer.__init__(self,options);
        self.name = 'ML-K-BAND-'+str(group_size);
#    function mci = ml_mst_inducer()
#        options_.structure_learner = @mst_structure_learner;
#        mci = ml_cgn_inducer.make('MAXIMUM SPANNING TREE',options_
