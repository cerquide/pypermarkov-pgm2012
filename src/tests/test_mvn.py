import numpy as np;
import numpy.random as nr;
import numpy.linalg as linalg;

import maths;
import mvn;
import mvt;
import normal_inverse_wishart;

def make_multidimensional_normal(n_vars):   

    #mu = [2,5];
    #sigma = [[3,0.7];[0.7,2]];

    mu = nr.randn(n_vars);

    s = nr.randn(n_vars,n_vars);
    sigma = maths.make_symmetric(np.dot(s.transpose(),s));
    ei = linalg.eig(sigma);
    while (np.any(ei<0)):
        s = nr.randn(n_vars)
        sigma = maths.make_symmetric(np.dot(s.transpose(),s));
        ei = linalg.eig(sigma)
    #print "real mu=",mu;
    #print "real sigma=",sigma;
    return mvn.mvn(mu,sigma);


if __name__ == '__main__':
    NVARS = 3
    m = make_multidimensional_normal(NVARS);
    train = m.sample(500000);
    test = m.sample(5000000);
    
    lpdf = m.logpdf(test);
    s = np.sum(lpdf);
    print 'Exact log score = ',s;
     
    t = mvt.mvt(1,m.mu,m.Sigma);
    lpdf = t.logpdf(test);
    s = np.sum(lpdf);
    print 't log score = ',s;

    t = mvt.mvt(100000,m.mu,m.Sigma);
    lpdf = t.logpdf(test);
    s = np.sum(lpdf);
    print 't log score = ',s;
