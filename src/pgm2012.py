#import cProfile;
import dataset;
import experiments;
import bma_gct_inducer;
import ml_cgn_inducer;

# This script runs the experiments for the PGM 2012 workshop paper

def load_experiment():
    h = experiments.experiment_handle('pgm2012','pgm2012.hdf5');
    e = experiments.experiment.open(h,'a');
    
#    # Add the ovarian-cancer CV task

#    inducers_list = [];
#    inducers_list.append(bma_gct_inducer.bma_k_jan_inducer(1));
#    inducers_list.append(ml_cgn_inducer.ml_k_jan_inducer(1));
#    for group_size in [2,3,4,5,10,13,14,15,16,17,18,19,20,25,28,29,30,31,32,35,40,45,50]:
#        inducers_list.append(bma_gct_inducer.bma_k_jan_inducer(group_size));
#        inducers_list.append(ml_cgn_inducer.ml_k_jan_inducer(group_size));
#        inducers_list.append(bma_gct_inducer.bma_k_band_inducer(group_size));
#        inducers_list.append(ml_cgn_inducer.ml_k_band_inducer(group_size));

#    ovarian = dataset.dataset_handle('ovarian-cancer','../data/ovarian_cancer.mat');

#    runs = 5
#    folds = 10;

#    task = experiments.cv_task('/','1CV10.1',ovarian,runs,folds,inducers_list);

#    e.overwrite_task(task);
  
    # Add the pancreas-cancer CV task

    inducers_list = [];
    inducers_list.append(bma_gct_inducer.bma_k_jan_inducer(1));
    inducers_list.append(ml_cgn_inducer.ml_k_jan_inducer(1));
    for group_size in [2,3,4,5,10,15,20]:
        inducers_list.append(bma_gct_inducer.bma_k_jan_inducer(group_size));
        inducers_list.append(ml_cgn_inducer.ml_k_jan_inducer(group_size));
        inducers_list.append(bma_gct_inducer.bma_k_band_inducer(group_size));
        inducers_list.append(ml_cgn_inducer.ml_k_band_inducer(group_size));

    pancreas = dataset.dataset_handle('pancreas-cancer','../data/pancreas_cancer.mat');

    runs = 1
    folds = 10;

    task = experiments.cv_task('/','pancreas',pancreas,runs,folds,inducers_list);

    e.add_task(task);  

    return e,task;

if __name__ == '__main__':
    e,task = load_experiment();
    #command = """e.work()"""
    #cProfile.runctx( command, globals(), locals(), filename="pgm2012.profile" )
    task.work(e.fileh);
    print "Experiment finished";
    e.close();
