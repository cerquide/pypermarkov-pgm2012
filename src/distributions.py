# Base class for probability distributions

import numpy as np;

class distribution:
    def logpdf(self,x):
        return np.zeros(x.shape[0]);

class conjugate_distribution(distribution):
    def incorporate_evidence(self,x):
        pass

    def get_posterior_predictive_distribution(self):
        pass

    def assess_posterior(self,x):
        posterior = self.deepcopy();
        posterior.incorporate_evidence(x);

class factor(distribution):
    def __init__(self,dim,domain,distribution):
        self.dim = dim;
        self.domain = domain;
        self.distribution = distribution;

    def logpdf(self,x):
        return self.distribution.logpdf(x[:,self.domain]);

    def jointDimension(self):
        return self.dim;

class conjugate_factor(factor,conjugate_distribution):
    def __init__(self,dim,domain,cd):
        #super(conjugate_factor,self).__init__(dim,domain,cd);
        factor.__init__(self,dim,domain,cd);
    
    def incorporate_evidence(self,x):
        self.distribution.incorporate_evidence(x[:,self.domain]);

    def get_posterior_predictive_distribution(self):
        # We should check here whether ppd is conjugate and return a conjugate_factor in that case.
        ppd = self.distribution.get_posterior_predictive_distribution();
        return factor(self.dim,self.domain,ppd);

class factored_distribution(distribution):
    def __init__(self,cliques,separators):
        self.dim = cliques[0].jointDimension();
        self.cliques = cliques;
        self.separators = separators;
    
    def logpdf(self,x):
        n = x.shape[0];
        logP = np.zeros(n);
        for clique in self.cliques:
            logP = logP + clique.logpdf(x);
        for separator in self.separators:
            logP = logP - separator.logpdf(x);
        return logP;

class strong_hypermarkov_distribution(factored_distribution,conjugate_distribution):
    def __init__(self,cliques,separators):
        factored_distribution.__init__(self,cliques,separators);

    def incorporate_evidence(self,x):
        self._incorporate_evidence_in_factors(self.cliques,x);
        self._incorporate_evidence_in_factors(self.separators,x);

    def get_posterior_predictive_distribution(self):
        cliques_ppd = self._get_ppd_factors(self.cliques);
        separators_ppd = self._get_ppd_factors(self.separators);
        return factored_distribution(cliques_ppd,separators_ppd);
        
    def _incorporate_evidence_in_factors(self,factors,x):
        for factor in factors:
            factor.incorporate_evidence(x);

    def _get_ppd_factors(self,factors):
        return [factor.get_posterior_predictive_distribution() for factor in factors];
        #n_factors = len(factors);
        #if (n_factors == 0):
        #    return [];
        #else:
        #    ppd_factors = [None]*n_factors;
        #    for factor_i in xrange(n_factors):
        #        ppd_factors(factor_i) = 
                

    
