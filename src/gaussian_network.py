from __future__ import division;
import numpy as np;
#import scipy.stats as ss;
#import scipy.linalg as linalg;
#import math;
import distributions;
import cond_normal;

class gaussian_network(distributions.distribution):
    def __init__(self,cds, structure):
        self.cond_distribs = cds;
        self.structure = structure;

    def logpdf(self, x):
        n_vars = len(self.cond_distribs);
        n_predictions = x.shape[0];
        logPredictions = np.zeros(n_predictions);
        for var_i in xrange(n_vars):
            cn = self.cond_distribs[var_i];
            xx = cn.logcpdf(x[:,self.structure.predecessors(var_i)],x[:,var_i]);
            logPredictions = logPredictions + xx;
        return logPredictions;

def learn_ml(x,structure):
    #print x
    n_vars = x.shape[1];
    cd = [None] * n_vars;
    for var_i in xrange(n_vars):
        cd[var_i] = cond_normal.learn_ml(x[:,structure.predecessors(var_i)],x[:,[var_i]]);
    return gaussian_network(cd,structure);
        

