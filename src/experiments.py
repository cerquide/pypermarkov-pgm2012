import tables;
from sklearn.cross_validation import KFold
import numpy as np;
import time;

import dataset;
import measures;

def load_task(d):
    if d.task_class == 'cv_task':
        t = cv_task()
        t.load(d);
    return t    

class task:
    def __init__(self,base="",name=""):
        self.base = base;
        self.name = name;
        self.where = self.base+self.name;
    
    def store(self,d):
        d.base = self.base;
        d.name = self.name;
        d.where = self.where;

    def load(self,d):
        self.base = d.base;
        self.name = d.name;
        self.where = d.where;
     
    #def load(self,node):
    #    pass

#class multidataset_cv_task(cv_task):
#    def __init__(self,name,dhandles,runs,folds,inducers):
#        cv_task.__init__(self,name)
#        self.dhandles = dhandles;
#        self.runs = runs;
#        self.folds = folds;
#        self.inducers = inducers;
#        self.restart();

#    def restart(self);
#        self.finished = False;
#        self.last_dataset = -1;
#        #self.last_finished_fold = -1;

#    def clear(self,taskNode):
#        self.restart();
#        #taskNode._v_attrs.task = self;
#        taskNode.flush();

#    def do_run(self,taskNode,run):
#        p = np.random.permutation(20);
#        f = KFold(20,3,indices=True)

#       cv_indxs = 
#                for folds in xrange(self.last_finished_fold + 1, self.folds):
#                    
#    def work(self,taskNode):
#        if not self.finished:
#            for run in xrange(self.last_finished_run + 1, self.runs):
#                self.do_run(taskNode,run);
#                taskNode.flush();
#            self.finished = True; 

class cv_task(task):
    def __init__(self,base="",name="",dhandle=None,runs=None,folds=None,inducers=None):
        task.__init__(self,base,name)
        self.dhandle = dhandle;
        self.runs = runs;
        self.folds = folds;
        self.inducers = inducers;
        self.restart();

    def store(self,d):
        task.store(self,d);
        d.task_class = 'cv_task';        
        d.dhandle = self.dhandle;
        d.runs = self.runs;
        d.folds = self.folds;
        d.inducers = self.inducers;
        d.finished = self.finished;
        d.last_finished_run = self.last_finished_run
        

    def load(self,d):
        task.load(self,d)
        self.dhandle = d.dhandle;
        self.runs = d.runs;
        self.folds = d.folds;
        self.inducers = d.inducers;
        self.finished = d.finished;
        self.last_finished_run = d.last_finished_run
        
    def restart(self):
        self.finished = False;
        self.last_finished_run = -1;
        #self.last_finished_fold = -1;

#    def clear(self,fileh):
#        self.restart();
#        #taskNode._v_attrs.task = self;
#        taskNode.flush();

    def get_run_group(self,fileh,run):    
        # Create group for the run
        # where  = self.where+'/'+str(run);
        run_name = 'run-'+str(run);
        run_group = None;
        try:
            run_group = fileh.getNode(self.where,run_name)
        except:
            run_group = fileh.createGroup(self.where,run_name);
        return run_group


    def get_fold_group(self,fileh,run_group,fold):
        # Create group for the fold_results
        #where = where + '/' + str(fold)
        fold_name = 'fold-'+str(fold);
        fold_group = None;
        try:
            fold_group = fileh.getNode(run_group,fold_name)
        except:
            fold_group = fileh.createGroup(run_group,fold_name);
        return fold_group

    def get_inducer_group(self,fileh,fold_group,inducer):
        # Create inducerNode
        #where = where + '/' + inducer.name;
        
        inducer_group = None;
        try:
            inducer_group = fileh.getNode(fold_group,inducer.name)
        except:
            inducer_group = fileh.createGroup(fold_group,inducer.name);
            inducer_group._v_attrs.finished = False
        return inducer_group

    def do_run(self,fileh,d,run):
        run_group = self.get_run_group(fileh,run)

        # Make a random permutation and add it as an attr of the group
        n_instances = d.str.n_instances;
        
        try:
            p = run_group.permutation
            #print "loaded"
        except:
            p = np.random.permutation(n_instances)
            #print "created"
            fileh.createArray(run_group,"permutation",p)
        #print "p=",p
        #exit()
        # Perform k fold cv
        cv = KFold(n_instances,self.folds,indices=True)
        fold = 0;
        for (train_idxs,test_idxs) in cv:
            self.do_fold(fileh,run_group,d,fold,p,train_idxs,test_idxs);
            fold = fold + 1;
        fileh.flush();
        #run_group.close();

    def do_fold(self,fileh,run_group,d,fold,p,train_idxs,test_idxs):
        fold_group = self.get_fold_group(fileh,run_group,fold)
    
        train_x = d.x[p[train_idxs],:];
        #print train_x.shape;
        #print train_x;
        train_y = d.y[p[train_idxs]];
        test_x = d.x[p[test_idxs],:];
        test_y = d.y[p[test_idxs]];
        for inducer in self.inducers:
            inducer_group = self.do_inducer(fileh,fold_group,d.str,train_x,train_y,test_x,inducer);
            inducer_group._v_attrs.cll = measures.cll(d.str,inducer_group.log_predictions,test_y);
            inducer_group._v_attrs.accuracy = measures.accuracy(d.str,inducer_group.log_predictions,test_y);
            print inducer_group
            print "CLL:",inducer_group._v_attrs.cll, " Accuracy:",inducer_group._v_attrs.accuracy
            fileh.flush()
        fileh.flush()
        #fold_group.close()

    def do_inducer(self,fileh,fold_group,dstr,train_x,train_y,test_x,inducer):
        inducer_group = self.get_inducer_group(fileh,fold_group,inducer)
        try:
            lp = inducer_group.log_predictions
            print "Done"
        except:
            print "To Do"
            start_learning = time.clock();
            model = inducer.learn(dstr,train_x,train_y)
            inducer_group._v_attrs.learning_time = time.clock() - start_learning;
            start_prediction = time.clock();
            log_predictions = model.predict(test_x)
            inducer_group._v_attrs.prediction_time = time.clock() - start_prediction;
            fileh.createArray(inducer_group,'log_predictions',log_predictions);
            inducer_group._v_attrs.finished = True
        return inducer_group
        
        #inducer_group.close()        

    

    def work(self,fileh):
        #if not self.finished:
        d = self.dhandle.load();
        #print self.last_finished_run
        #print self.last_finished_run + 1
        #print self.runs
        for run in xrange(self.last_finished_run + 1, self.runs):
            self.do_run(fileh,d,run);
            fileh.flush();
        self.finished = True; 
        fileh.flush();  

    def get_results(self,fileh,measures):
        n_inducers = len(self.inducers);
        m = {}
        #np.zeros([2,self.runs,self.folds,n_inducers])
        run = 0
        for run_group in fileh.iterNodes(self.where):
            print run_group
            fold = 0;
            for fold_group in fileh.iterNodes(run_group):
                print fold_group
                if fold_group._v_name == "permutation":
                    continue;
                for inducer_group in fileh.iterNodes(fold_group):
                    if fold_group._v_name == "log_predictions":
                        continue;
                    if inducer_group._v_name not in m:
                        #print "Runs:",self.runs
                        m[inducer_group._v_name]= np.zeros([len(measures),self.runs,self.folds]);
                    for (m_i,measure) in enumerate(measures):          
                        m[inducer_group._v_name][m_i,run,fold] = getattr(inducer_group._v_attrs,measure)
                fold = fold + 1
            run = run + 1
            if run == self.runs:
                break;
        return m 
        #print m

    def assess_summaries(self,fileh):
        m = self.get_results(fileh)
        a = m.keys()
        a.sort()
        for mi in a:
            print mi
            av = np.average(m[mi],axis=1);
            print np.average(av,axis=1);
            #m2 = np.average(m,axis=1);
        #print m2;
        #m3 = np.average(m2,axis=1);
        #print m3;

        

class experiment_handle:
    def __init__(self,name,filename):
        self.name = name;
        self.filename = filename;

class experiment:
    def __init__(self,filename,mode_):
        self.filename = filename;
        fileh = tables.openFile(filename, mode = mode_) 
        self.fileh = fileh;
        self.root = fileh.root;
        
    def overwrite_task(self,task):
        if task.where in self.fileh:
            taskNode = self.fileh.getNode(self.root,task.where)
            task.store(taskNode._v_attrs);
            #taskNode._v_attrs.task = task;
            #taskNode.close();            
            #self.root.__set__(task.name,task);

    def add_task(self,task):
        #print task.where
        #print self.fileh
        #print self.fileh.listNodes("/")
        if not task.where in self.fileh:
            print "Adding new task"
            taskNode = self.fileh.createGroup(task.base,task.name);
            task.store(taskNode._v_attrs);
            #taskNode._v_attrs.task = task;
            #taskNode.close();       
    def get_task(self,task_name):
        taskNode = self.fileh.getNode(self.root,task_name);
        task = load_task(taskNode._v_attrs)
        return task  

    def clear(self):
        pass

    def work(self):
        for taskNode in self.fileh.iterNodes(self.root):
            task = load_task(taskNode._v_attrs);
            #taskNode._v_attrs.
            task.work(self.fileh);
            task.store(taskNode._v_attrs);
            self.fileh.flush()
    
    def assess_summaries(self):
         for taskNode in self.fileh.iterNodes(self.root):
            task = load_task(taskNode._v_attrs);
            #taskNode._v_attrs.
            task.assess_summaries(self.fileh);
            #task.store(taskNode._v_attrs);
            #self.fileh.flush()

    def open(experiment_handle,mode):
        return experiment(experiment_handle.filename,mode);

    open = staticmethod(open);

    def close(self):
        self.fileh.close();


