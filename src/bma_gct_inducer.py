from __future__ import division 
import numpy as np;
import math

import dataset;
import gaussian_clique_tree;
import mixture_model;
import inducers;

def naive_bayes_ct_learner(dstr,xtrain,ytrain,options):
    print dstr;
    clique_domains = [None] * dstr.n_vars
    separator_domains = [];
    for var_i in xrange(dstr.n_vars):
        clique_domains[var_i] = [var_i];
    return (clique_domains,separator_domains)

def k_jan_ct_learner(dstr,xtrain,ytrain,options):
    group_size = options['group_size'];
    n_groups = int(math.ceil(dstr.n_vars/group_size));
    clique_domains = [None] * n_groups
    for group_i in xrange(n_groups):
        start = group_i*group_size
        clique_domains[group_i] = range(start,min(dstr.n_vars,start+group_size))
    return (clique_domains,[])
    
def k_band_ct_learner(dstr,xtrain,ytrain,options):
    group_size = options['group_size'];
    n_cliques = dstr.n_vars - group_size + 1;
    clique_domains = [None] * n_cliques;
    separator_domains = [None] * (n_cliques - 1);
    #print 'n_groups=',n_groups
    #parents = [None] * dstr.n_vars
    
    for var_i in range(n_cliques-1):
        end_clique = var_i + group_size
        clique_domains[var_i] = range(var_i,var_i + group_size);
        separator_domains[var_i] = range(var_i+1,var_i + group_size);   
    clique_domains[n_cliques-1] = range(n_cliques-1,n_cliques-1 + group_size);
    return clique_domains, separator_domains

ct_learners = {};
ct_learners['naive_bayes'] = naive_bayes_ct_learner;
ct_learners['k_jan'] = k_jan_ct_learner;
ct_learners['k_band'] = k_band_ct_learner;


class bma_gct_inducer(inducers.inducer):
    def __init__(self,options):
        self.options = options;
        if (not self.has_learner()):
            self.options['ct_learner'] = 'naive_bayes'; 

    def has_learner(self):
        return self.options.has_key('ct_learner');
    
    #def _set_learner(self,leaner):
    #    self.options['ct_learner'] = learner;
    
    #def _get_learner(self):
    #    return self.options['ct_learner'];

    #learner = property(_get_learner,_set_learner)
    
    def learn(self,dstr,xtrain,ytrain):
        (clique_domains,separator_domains) = ct_learners[self.options['ct_learner']](dstr,xtrain,ytrain,self.options);
        model = self.learn_parameters(dstr,xtrain,ytrain,clique_domains,separator_domains,self.options);
        return model;
       
    def learn_parameters(self,dstr,xtrain,ytrain,clique_domains,separator_domains,options):
            
        #print xtrain.shape
        # Assess the unconditional probabilities for each class.
        model_theta = np.zeros(dstr.n_classes);
        for class_i in range(dstr.n_classes):
            model_theta[class_i] = np.count_nonzero(ytrain==dstr.class_values[class_i])+1; # Here we apply the Dirichlet prior with 1.
        model_theta = model_theta/np.sum(model_theta);

        # Divide the training set in classes
        inst_by_class = dataset.split_by_classes(dstr,xtrain,ytrain);
        
        # Assess the parameters for each of the classes
        model_ts = [None] * dstr.n_classes;

        for class_i in range(dstr.n_classes):
            #print inst_by_class[class_i].shape;
            #print inst_by_class[class_i];
            gct = gaussian_clique_tree.prior(dstr.n_vars, clique_domains, separator_domains);
            gct.incorporate_evidence(inst_by_class[class_i]);
            model_ts[class_i] = gct.get_posterior_predictive_distribution();       
        return mixture_model.mixture_model(model_theta,model_ts);


class bma_k_jan_inducer(bma_gct_inducer):
    def __init__(self,group_size):
        options = {};
        options['group_size'] =  group_size;
        options['ct_learner'] = 'k_jan';
        bma_gct_inducer.__init__(self,options);
        self.name = 'BMA-K-JAN-'+str(group_size);
        #self.group_size = group_size;

class bma_k_band_inducer(bma_gct_inducer):
    def __init__(self,group_size):
        options = {};
        options['group_size'] =  group_size;
        options['ct_learner'] = 'k_band';
        bma_gct_inducer.__init__(self,options);
        self.name = 'BMA-K-BAND-'+str(group_size);
        #self.group_size = group_size;


