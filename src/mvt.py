from __future__ import division;
import numpy as np;

import math;
import numpy.linalg as linalg;
import scipy.special as ss;

from distributions import *;
import maths;

class mvt(distribution):
    def __init__(self,nu,mu,Sigma):
        self.dim = mu.shape[0];
        self.nu = nu;
        self.mu = mu;
        self.Sigma = Sigma;
        self.log_constant = self._assess_log_constant();
        #print "ctt:",self.log_constant;

    def _assess_log_constant(self):
        ctte = ss.gammaln((self.nu + self.dim) / 2) - ss.gammaln(self.nu/2) - (self.dim/2) * math.log(self.nu * math.pi);
        (s,l) =  linalg.slogdet(self.Sigma);
        return ctte - 0.5 * l; 

    def logpdf(self,x):
        return self.log_constant - ((self.nu + self.dim)/2) * np.log1p(maths.quadratic_form_of_inverse(self.Sigma,x - self.mu)/self.nu) 

if __name__ == '__main__':
    from mvt import *
    S= np.array([[1,0.2],[0.2,1]]);
    #t = mvt(1,np.array([1,4]),S);
    t2 = mvt(2,np.array([1,4]),S);
    #t3 = mvt(3,np.array([1,4]),S);
    s = np.array([[1,4],[2,6]])
    #print "t1 ",t.logpdf(s);
    #print "t2 ",t2.logpdf(s);
    sol = np.array([-1.81746607,-4.13593989]);
    sol2 = t2.logpdf(s);
    #print sol
    #print sol2
    #print linalg.norm(sol-sol2);
    assert (linalg.norm(sol-sol2)<1e-9);
    

